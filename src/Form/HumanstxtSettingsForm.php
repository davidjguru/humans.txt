<?php

/**
 * @file
 * Contains \Drupal\humanstxt\Form\HumanstxtSettingsForm.
 */

namespace Drupal\humanstxt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Configure maintenance settings for this site.
 */
class HumanstxtSettingsForm extends ConfigFormBase {
  protected $config;
  protected $textBox;
  protected $linkCheck;
  protected $defaultValue;
  protected $setValue;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'humanstxt_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'system.site',
      'user.mail',
      'user.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // We're getting all the config values from database.
    $this->config = $this->configFactory->get('humanstxt.settings');
    $this->textBox = $this->config->get('content');
    $this->link_check = $this->config->get('display_link');
    $this->defaultValue = <<<VALUE
/* TEAM */

/* SITE */
Tools:Drupal
VALUE;
    // If the main value is empty in config table from database, set default.
    if (empty($this->textBox)) {
      $this->setValue = $this->defaultValue;
    }
    // In other case, show the stored value.
    else {
      $this->setValue = $this->textBox;
    }
    // Build the main form for Humans.txt.
    $form['about'] = [
      '#markup' => $this->t('Add here the information about the different people who
      have contributed to building the website, you can find more info in <a
      href="@humanstxt">humanstxt.org</a> and use
      <a href="http://humanstxt.org/humans.txt">this file</a> as base file.',
      ['@humanstxt' => 'http://humanstxt.org']),
    ];
    $form['humanstxt_content'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Contents of humans.txt'),
      '#default_value' => $this->setValue,
      '#cols' => 60,
      '#rows' => 20,
    ];
    $form['humanstxt_display_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display humans.txt in head section as a link.'),
      '#description' => $this->t('Activating this setting will make humans.txt file
      to be linked in the head section of the html code'),
      '#default_value' => $this->config->get('display_link'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate Humans.txt File'),
      '#button_type' => 'primary',
      '#prefix' => '<div class="submit-value"></div>',
      '#suffix' => '<p>Through this action, you will generate a new Humans.txt
       file that will overwrite the previous one if it already exists.</p>',
      '#submit' => [$this, createHumanstxtFile],
    ];

    return $form;
  }

  public function createHumanstxtFile(array $form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();
    if ((empty(($form_state)->getValue('humanstxt_content')) === FALSE)) {
      $text = 'SEND CACAFUTI';
    }
    elseif (empty(($form_state)->getValue('humanstxt_content'))) {
      $text = 'Esto está en blanco, CABESA';
      // Load an error message.
      $form_state->setErrorByName('humanstxt_content', $this->t('The main
      field of content for Humans.txt can\'t be empty.'));
      // Back to set the default value in the field.
      $form['humanstxt_content']['#value'] = $this->defaultValue;
    }
    $ajax_response->addCommand(new HtmlCommand('.submit-value', $text));
    return $ajax_response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Our goal is avoid the main text field is being recorded empty.
    if (empty(($form_state)->getValue('humanstxt_content'))) {
      // Load an error message.
      $form_state->setErrorByName('humanstxt_content', $this->t('The main
      field of content for Humans.txt can\'t be empty.'));
      // Back to set the default value in the field.
      $form['humanstxt_content']['#value'] = $this->defaultValue;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('humanstxt.settings')
      ->set('content', $form_state->getValue('humanstxt_content'))
      ->set('display_link', $form_state->getValue('humanstxt_display_link'))
      ->save();
    drupal_set_message($this->t('Ok, the fields have been saved and the
    Humans.txt file has been created.'));
    foreach ($form_state->getValues() as $key => $value) {
      if (($key == 'humanstxt_content') || ($key == 'humanstxt_display_link')) {
      drupal_set_message($key . ': ' . $value);
      }
    }
  }

}
